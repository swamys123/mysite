# Generated by Django 2.0.4 on 2019-05-04 11:11

import datetime
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import student.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()])),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('dept', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=70)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=datetime.datetime(2019, 5, 4, 11, 11, 55, 288887, tzinfo=utc), verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name_plural': 'users',
                'verbose_name': 'user',
            },
            managers=[
                ('objects', student.models.CustomUserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Attendence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('regno', models.IntegerField()),
                ('rollno', models.IntegerField()),
                ('name', models.CharField(default='', max_length=50)),
                ('section', models.CharField(choices=[('A', 'A'), ('B', 'B')], max_length=1)),
                ('year', models.IntegerField(choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024), (2025, 2025), (2026, 2026), (2027, 2027), (2028, 2028)])),
                ('branch', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication Engineering'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=50)),
                ('cursem', models.CharField(choices=[('1', 'S1'), ('2', 'S2'), ('3', 'S3'), ('4', 'S4'), ('5', 'S5'), ('6', 'S6'), ('7', 'S7'), ('8', 'S8')], default='0', max_length=4, verbose_name='Semester')),
                ('sub1', models.IntegerField()),
                ('sub2', models.IntegerField()),
                ('sub3', models.IntegerField()),
                ('sub4', models.IntegerField()),
                ('sub5', models.IntegerField()),
                ('sub6', models.IntegerField()),
                ('lab1', models.IntegerField()),
                ('lab2', models.IntegerField()),
                ('lab3', models.IntegerField()),
                ('lab4', models.IntegerField()),
                ('ptotal', models.IntegerField(default='0')),
                ('atotal', models.IntegerField(default='0')),
                ('ftotal', models.IntegerField(default='0')),
                ('time', models.DateTimeField(default=datetime.datetime(2019, 5, 4, 11, 11, 55, 294397, tzinfo=utc))),
            ],
        ),
        migrations.CreateModel(
            name='Faculty',
            fields=[
                ('photo', models.FileField(blank=True, null=True, upload_to=student.models.upload_location2)),
                ('empid', models.IntegerField(primary_key=True, serialize=False)),
                ('ename', models.CharField(max_length=30)),
                ('dept', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=70, verbose_name='Department')),
                ('dob', models.DateField(help_text='dd-mm-yyyy')),
                ('desig', models.CharField(choices=[('Associate Professor', 'Associate Professor'), ('Assistant Professor', 'Assistant Professor')], default='Prof', max_length=20, verbose_name='Designation')),
                ('permaddr', models.TextField(max_length=50)),
                ('tempaddr', models.TextField(max_length=50)),
                ('category', models.CharField(choices=[('Permanent', 'Permanent'), ('Temporary', 'Temporary')], default='Temporary', max_length=20)),
                ('email', models.EmailField(max_length=254)),
                ('contact', models.CharField(max_length=13)),
                ('status', models.CharField(max_length=10)),
                ('datejoin', models.DateField(blank=True, null=True)),
                ('dateresig', models.DateField(blank=True, null=True)),
            ],
            options={
                'ordering': ('empid',),
            },
        ),
        migrations.CreateModel(
            name='FacultySubject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sem', models.CharField(choices=[('1', 'S1'), ('2', 'S2'), ('3', 'S3'), ('4', 'S4'), ('5', 'S5'), ('6', 'S6'), ('7', 'S7'), ('8', 'S8')], default='0', max_length=2, verbose_name='Semester')),
                ('branch', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication Engineering'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], default='A', max_length=50, verbose_name='Branch')),
                ('section', models.CharField(choices=[('A', 'A'), ('B', 'B')], default='A', max_length=1, verbose_name='Section')),
                ('year', models.IntegerField(choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024), (2025, 2025), (2026, 2026), (2027, 2027), (2028, 2028)], verbose_name='Year Of Join')),
                ('empid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.Faculty')),
            ],
            options={
                'ordering': ('empid',),
            },
        ),
        migrations.CreateModel(
            name='Marklist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('branch', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=70)),
                ('cursem', models.CharField(choices=[('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8')], max_length=2)),
                ('section', models.CharField(choices=[('A', 'A'), ('B', 'B')], max_length=1)),
                ('type', models.CharField(choices=[('Internal', 'Internal'), ('External', 'External')], default='Internal', max_length=9)),
                ('join', models.IntegerField(choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024), (2025, 2025), (2026, 2026), (2027, 2027), (2028, 2028)], default='0')),
                ('chance', models.IntegerField(default='0')),
                ('mark1', models.SmallIntegerField(default='0')),
                ('mark2', models.SmallIntegerField(default='0')),
                ('mark3', models.SmallIntegerField(default='0')),
                ('mark4', models.SmallIntegerField(default='0')),
                ('subcode5', models.CharField(blank=True, default='0', max_length=15, null=True)),
                ('mark5', models.SmallIntegerField(default='0')),
                ('subcode6', models.CharField(blank=True, default='0', max_length=15, null=True)),
                ('mark6', models.SmallIntegerField(blank=True, default='0', null=True)),
                ('markl1', models.SmallIntegerField(default='0')),
                ('markl2', models.SmallIntegerField(default='0')),
                ('subcodel3', models.CharField(blank=True, default='0', max_length=15, null=True)),
                ('markl3', models.SmallIntegerField(blank=True, default='0', null=True)),
                ('subcodel4', models.CharField(blank=True, default='0', max_length=15, null=True)),
                ('markl4', models.SmallIntegerField(blank=True, default='0')),
            ],
            options={
                'ordering': ('regno',),
            },
        ),
        migrations.CreateModel(
            name='RollnoRegnoMap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('regno', models.IntegerField()),
                ('rollno', models.IntegerField()),
                ('name', models.CharField(default='', max_length=50)),
                ('section', models.CharField(choices=[('A', 'A'), ('B', 'B')], max_length=1)),
                ('year', models.IntegerField(choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024), (2025, 2025), (2026, 2026), (2027, 2027), (2028, 2028)])),
                ('branch', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication Engineering'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=50)),
                ('cursem', models.CharField(choices=[('1', 'S1'), ('2', 'S2'), ('3', 'S3'), ('4', 'S4'), ('5', 'S5'), ('6', 'S6'), ('7', 'S7'), ('8', 'S8')], default='0', max_length=4, verbose_name='Semester')),
                ('firsthr', models.CharField(choices=[('N', 'Null'), ('P', 'Present'), ('A', 'Absent')], default='Null', max_length=10)),
                ('secondhr', models.CharField(choices=[('N', 'Null'), ('P', 'Present'), ('A', 'Absent')], default='Null', max_length=10)),
                ('thirdhr', models.CharField(choices=[('N', 'Null'), ('P', 'Present'), ('A', 'Absent')], default='Null', max_length=10)),
                ('fourthhr', models.CharField(choices=[('N', 'Null'), ('P', 'Present'), ('A', 'Absent')], default='Null', max_length=10)),
                ('fifthhr', models.CharField(choices=[('N', 'Null'), ('P', 'Present'), ('A', 'Absent')], default='Null', max_length=10)),
                ('sixthhr', models.CharField(choices=[('N', 'Null'), ('P', 'Present'), ('A', 'Absent')], default='Null', max_length=10)),
                ('ptotal', models.IntegerField(default='-1')),
                ('atotal', models.IntegerField(default='-1')),
                ('ftotal', models.IntegerField(default='-1')),
                ('time', models.DateTimeField(default=datetime.datetime(2019, 5, 4, 11, 11, 55, 294998, tzinfo=utc))),
                ('facultyid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.Faculty', verbose_name='Facuty Id')),
            ],
            options={
                'ordering': ('id',),
                'get_latest_by': 'time',
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('photo', models.FileField(blank=True, null=True, upload_to=student.models.upload_location)),
                ('regno', models.IntegerField(primary_key=True, serialize=False, verbose_name='Register Number')),
                ('name', models.CharField(max_length=30, verbose_name='Name')),
                ('branch', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=15)),
                ('cursem', models.CharField(choices=[('1', 'S1'), ('2', 'S2'), ('3', 'S3'), ('4', 'S4'), ('5', 'S5'), ('6', 'S6'), ('7', 'S7'), ('8', 'S8')], max_length=2, verbose_name='Semester')),
                ('join', models.IntegerField(choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024), (2025, 2025), (2026, 2026), (2027, 2027), (2028, 2028)], verbose_name='Year Of Join')),
                ('section', models.CharField(choices=[('A', 'A'), ('B', 'B')], max_length=1)),
                ('status', models.CharField(choices=[('Active', 'Currently Studying'), ('Pass out', 'Course Completed'), ('Drop out', 'Drop out ')], default='Active', max_length=20)),
                ('admtype', models.CharField(choices=[('Regular', 'Regular'), ('Lateral Entry', 'Lateral Entry')], default='Regular', max_length=20)),
                ('gender', models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='M', max_length=6)),
                ('admissionno', models.IntegerField(default='0', verbose_name='Admission Number')),
                ('permanentaddress', models.CharField(blank=True, max_length=150, null=True, verbose_name='Permanent Address')),
                ('temporaryaddress', models.CharField(blank=True, max_length=150, null=True, verbose_name='Temporary Address')),
                ('dateofbirth', models.DateField(default='2000-01-01')),
                ('category', models.CharField(default='General', max_length=20)),
                ('emailid', models.CharField(blank=True, max_length=80, null=True, verbose_name='Email Id')),
                ('personwithdisabilities', models.CharField(blank=True, max_length=40, null=True, verbose_name='Person with Disabilities')),
                ('catrank', models.IntegerField(default='0', verbose_name='CAT Rank')),
                ('religion', models.CharField(blank=True, max_length=15, null=True)),
                ('bloodgroup', models.CharField(choices=[('A+ve', 'A+ve'), ('A-ve', 'A-ve'), ('B+ve', 'B+ve'), ('B-ve', 'B-ve'), ('AB+ve', 'AB+ve'), ('AB-ve', 'AB-ve'), ('O+ve', 'O+ve'), ('O-ve', 'O-ve')], default='Op', max_length=6, verbose_name='Blood Group')),
                ('parentorguardianname', models.CharField(blank=True, max_length=50, null=True, verbose_name='Parent/Guardians Name')),
                ('parentorguardianoccupation', models.CharField(blank=True, max_length=15, null=True, verbose_name='Parent/Guardians Occupation')),
                ('parentorguardiancontactno', models.CharField(blank=True, max_length=15, null=True, verbose_name='Parent/Guardians Contact No')),
                ('parentorguardianemailid', models.CharField(blank=True, max_length=50, null=True, verbose_name='Parent/Guardians Email Id')),
                ('miniproject', models.CharField(blank=True, max_length=100, null=True, verbose_name='Mini Project')),
                ('miniprojectguide', models.CharField(blank=True, max_length=25, null=True, verbose_name='Mini Project Guide')),
                ('mainproject', models.CharField(blank=True, max_length=100, null=True, verbose_name='Main Project')),
                ('mainprojectguide', models.CharField(blank=True, max_length=25, null=True, verbose_name='Main Project Guide')),
                ('behaviour', models.CharField(default='Good', max_length=100)),
                ('studentcontactno', models.CharField(blank=True, max_length=15, null=True, verbose_name='Students Contact No')),
                ('studentemailid', models.CharField(blank=True, max_length=50, null=True, verbose_name='Students Email Id')),
                ('tenboard', models.CharField(blank=True, max_length=20, null=True, verbose_name='10th Board')),
                ('tenregisterno', models.IntegerField(blank=True, null=True, verbose_name='10th Register No')),
                ('tenmarks', models.IntegerField(blank=True, null=True, verbose_name='10th Marks')),
                ('tenpercentage', models.IntegerField(blank=True, null=True, verbose_name='10th Percentage')),
                ('tenyear', models.IntegerField(default='0')),
                ('qualifyingboard', models.CharField(blank=True, max_length=20, null=True, verbose_name='Qualifying Board')),
                ('qualifyingregisterno', models.IntegerField(blank=True, null=True, verbose_name='Qualifying Register No')),
                ('qualifyingmarks', models.IntegerField(blank=True, null=True, verbose_name='Qualifying Marks')),
                ('qualifyingpercentage', models.IntegerField(blank=True, null=True, verbose_name='Qualifying Percentage')),
                ('qualifyingyear', models.IntegerField(blank=True, null=True, verbose_name='Qualifying Year')),
                ('specialreservation', models.CharField(blank=True, max_length=30, null=True, verbose_name='Special Reservation')),
            ],
            options={
                'ordering': ('regno',),
            },
        ),
        migrations.CreateModel(
            name='Subject_Profile',
            fields=[
                ('code', models.CharField(max_length=15, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=70)),
                ('branch', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=70)),
                ('sem', models.CharField(choices=[('1', 'S1'), ('2', 'S2'), ('3', 'S3'), ('4', 'S4'), ('5', 'S5'), ('6', 'S6'), ('7', 'S7'), ('8', 'S8')], max_length=2)),
                ('stype', models.CharField(choices=[('T', 'Theory'), ('L', 'Practical'), ('P', 'Project')], max_length=15)),
                ('credit', models.IntegerField(default=3)),
            ],
            options={
                'ordering': ('code',),
            },
        ),
        migrations.CreateModel(
            name='Syllabus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.PositiveIntegerField(choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024), (2025, 2025), (2026, 2026), (2027, 2027), (2028, 2028)])),
                ('dept', models.CharField(choices=[('CS', 'Computer Science And Engineering'), ('IT', 'Information Technology'), ('EEE', 'Electrical & Electronics Engineering'), ('EC', 'Electronics & Communication Engineering'), ('SFE', 'Safety & Fire Engineering'), ('CE', 'Civil Engineering'), ('ME', 'Mechanical Engineering')], max_length=60, verbose_name='Department')),
                ('theory_max_internal', models.PositiveIntegerField()),
                ('theory_max_external', models.PositiveIntegerField()),
                ('lab_max_internal', models.PositiveIntegerField()),
                ('lab_max_external', models.PositiveIntegerField()),
            ],
            options={
                'ordering': ('year',),
            },
        ),
        migrations.AddField(
            model_name='subject_profile',
            name='syllabussubid',
            field=models.ForeignKey(default='1', on_delete=django.db.models.deletion.CASCADE, to='student.Syllabus'),
        ),
        migrations.AddField(
            model_name='rollnoregnomap',
            name='subjectcode',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.Subject_Profile', verbose_name='Subject Code'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='regno',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='student.Student'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='subcode1',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Subcode1', to='student.Subject_Profile'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='subcode2',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Subcode2', to='student.Subject_Profile'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='subcode3',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Subcode3', to='student.Subject_Profile'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='subcode4',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Subcode4', to='student.Subject_Profile'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='subcodel1',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Subcodel1', to='student.Subject_Profile'),
        ),
        migrations.AddField(
            model_name='marklist',
            name='subcodel2',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Subcodel2', to='student.Subject_Profile'),
        ),
        migrations.AddField(
            model_name='facultysubject',
            name='subcode',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='student.Subject_Profile', verbose_name='Subject Code'),
        ),
    ]
